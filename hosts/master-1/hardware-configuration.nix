{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/68c7758a-16c7-4060-a856-b0b89d95cc75";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/EAB8-F1F8";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/745d157c-cead-4fd9-8c43-a2531eebc778"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
