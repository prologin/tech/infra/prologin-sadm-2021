{ lib, ... }:

{
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    forwardX11 = true;
    challengeResponseAuthentication = false;
    extraConfig = lib.mkBefore ''
      AllowUsers root
      PermitEmptyPasswords no
    '';
  };

  services.netdata = {
    enable = true;
  };
}
