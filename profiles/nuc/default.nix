{ pkgs, ... }:

{
  systemd.services.nic-configuration = {
    description = "Configuration of the eno1 NIC";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    serviceConfig.RemainAfterExit = true;
    script = ''
      # Offloading makes some of the NIC we use flap
      ${pkgs.ethtool}/bin/ethtool --offload eno1 rx off tx off tso off gso off
      # Ethernet PAUSE frame should not be used because it confuses TCP flow
      # control.  Some switches emit them, we disable their support on our
      # systems.  Some NIC do not implement this feature, so this commmand may
      # fail with "Operation not supported". This is totally fine.
      ${pkgs.ethtool}/bin/ethtool --pause eno1 rx off tx off
    '';
  };
}
