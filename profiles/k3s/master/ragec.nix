{ config, pkgs, lib, ... }:

with lib;

{
  systemd.services.ragec = {
    description = "Régie Autonome de la Gestion d'Émission des Certificats";
    after = [ "network.target" ];
    environment = {
      inherit (config.environment.variables) KUBECONFIG;
    };
    serviceConfig = {
      ExecStart = "${pkgs.ragec}/bin/ragec";
      Type = "oneshot";
      EnvironmentFile = config.sops.secrets.cloudflare_token.path;
    };
  };

  sops.secrets.cloudflare_token.sopsFile = ./cloudflare.yml;
}
