{ config, lib, pkgs, ... }:

let
  certs = config.services.kubernetes.pki.certs;
in
{
  sops.secrets =
    let
      commonSopsOptions = {
        owner = "etcd";
        sopsFile = ../certs/certs.yml;
      };
    in
    {
      master-1-key = commonSopsOptions;
      master-2-key = commonSopsOptions;
      master-3-key = commonSopsOptions;
      peer-master-1-key = commonSopsOptions;
      peer-master-2-key = commonSopsOptions;
      peer-master-3-key = commonSopsOptions;
    };

  networking.firewall.allowedTCPPorts = [ 2379 2380 ];
  services.etcd = {
    enable = true;
    initialAdvertisePeerUrls = [
      "https://${config.deploy.ip}:2380"
    ];
    listenPeerUrls = [
      "https://${config.deploy.ip}:2380"
    ];
    listenClientUrls = [
      "https://${config.deploy.ip}:2379"
    ];
    advertiseClientUrls = [
      "https://${config.deploy.ip}:2379"
    ];
    initialCluster = [
      "master-1=https://10.224.33.5:2380"
      "master-2=https://10.224.33.6:2380"
      "master-3=https://10.224.33.7:2380"
    ];

    clientCertAuth = true;
    trustedCaFile = ../certs/ca.pem;
    certFile = "${toString ../certs}/${config.services.etcd.name}.pem";
    keyFile = config.sops.secrets."${config.services.etcd.name}-key".path;
    peerClientCertAuth = true;
    peerTrustedCaFile = ../certs/ca.pem;
    peerCertFile = "${toString ../certs}/peer-${config.services.etcd.name}.pem";
    peerKeyFile = config.sops.secrets."peer-${config.services.etcd.name}-key".path;
  };
}
