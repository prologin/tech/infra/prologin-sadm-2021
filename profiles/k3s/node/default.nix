{ lib, pkgs, inputs, ... }:

{
  boot.supportedFilesystems = [ "nfs" ];

  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = lib.mkDefault true;
    "net.ipv4.conf.eno1.send_redirects" = false;
    "net.ipv6.conf.all.forwarding" = true;
    "net.ipv4.ip_nonlocal_bind" = true;
    "net.ipv4.conf.lxc*.rp_filter" = 0;
  };

  environment.persistence."/srv" = {
    directories = [
      "/etc/rancher"
      "/var/lib/rancher"
      "/var/lib/docker"
      "/etc/salt/pki"
    ];
  };

  networking.dhcpcd.denyInterfaces = [ "veth*" "flannel*" "cni*" "cal*" "docker*" ];
  networking.firewall.allowedTCPPorts = [
    7946 # metallb
    7472 # metallb
    10250 # kubelet metrics
    65001 # dragonfly
  ];
  networking.firewall.allowedUDPPorts = [
    8472 # flannel
  ];

  virtualisation.docker = {
    logDriver = "json-file";
    extraOptions = "--registry-mirror http://127.0.0.1:65001 --live-restore";
  };

  networking.firewall.enable = false;

  environment.systemPackages = [ pkgs.iptables ];

  services.k3s = {
    enable = true;
    serverAddr = "https://10.224.33.248:6443";
    docker = true;
  };

  virtualisation.oci-containers = {
    containers = {
      dragonflyoss = {
        image = "registry.gitlab.com/risson/prologin-sadm-2021/dragonflyoss/dfclient:0.3.1";
        imageFile = pkgs.dockerTools.pullImage {
          imageName = "registry.gitlab.com/risson/prologin-sadm-2021/dragonflyoss/dfclient";
          imageDigest = "sha256:35843fd0fe8aeadf03734501ca3c6ab26c3fcaddb3dba008d2c45c82eca9308d";
          finalImageTag = "0.3.1";
          sha256 = "sha256-P0FCEuRaQwV7lCEVUDC/7ejUSDEH+mF5tVNu2zFOinY=";
        };
        cmd = [ "--node" "10.224.33.2:8002" ];
        ports = [ "65001" ];
        volumes = [ "/srv/dragonfly:/root/.small-dragonfly" ];
        extraOptions = [ "--network=host" ];
      };
    };
  };
}
