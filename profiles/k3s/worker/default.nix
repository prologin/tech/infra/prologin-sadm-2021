{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.k3s;
in
{
  services.k3s = {
    role = "agent";
    token = "stub";
  };

  systemd.services.k3s.serviceConfig.ExecStart = mkForce (pkgs.writeShellScript "k3s.sh" ''
    exec ${cfg.package}/bin/k3s ${cfg.role} \
      ${optionalString cfg.docker "--docker"} \
      ${optionalString cfg.disableAgent "--disable-agent"} \
      --server ${cfg.serverAddr} \
      --token-file /var/lib/rancher/k3s_token \
      --no-flannel \
      --pause-image registry.gitlab.com/risson/prologin-sadm-2021/rancher/pause:3.1
  '');

  environment.systemPackages = singleton (pkgs.writeScriptBin "nixos-kubernetes-node-join" ''
    set -e
    exec 1>&2
    if [ $# -gt 0 ]; then
      echo "Usage: $(basename $0)"
      echo ""
      echo "No args. k3s token must be provided on stdin."
      exit 1
    fi
    if [ $(id -u) != 0 ]; then
      echo "Run as root please."
      exit 1
    fi
    read -r token
    echo $token > /var/lib/rancher/k3s_token
    chmod 600 /var/lib/rancher/k3s_token
    echo "Restarting k3s..." >&1
    systemctl restart k3s
    echo "Node joined succesfully"
  '');
}
