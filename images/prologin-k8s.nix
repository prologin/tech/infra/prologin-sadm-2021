{ inputs, pkgs, lib, config, ... }:

{
  imports = [
    inputs.self.nixosModules.profiles.server
    inputs.self.nixosModules.profiles.nuc
    inputs.self.nixosModules.profiles.k3sNode
    inputs.self.nixosModules.profiles.k3sWorker
  ];

  netboot = {
    enable = true;
    torrent = {
      webseed = {
        url = "https://prologin.org/static/epita-pie/";
      };
    };
  };

  fileSystems = {
    "/srv" = {
      fsType = "ext4";
      device = "/dev/disk/by-partlabel/srv";
      neededForBoot = true;
    };
  };

  cri = {
    aria2.enable = true;
    salt = {
      enable = true;
      master = "10.224.33.2";
    };
    sshd.enable = true;
  };

  systemd.services.salt-minion = {
    path = [ pkgs.inetutils ];
    preStart = lib.mkForce ''
      while true; do
        ip="$(ip a | grep 'inet ' | grep -v '127.0.0.1' | head -n1 | awk '{print $2}' | sed 's#/.*$##')"
        if [ -n "$ip" ] ; then
          break
        fi
        sleep 2
      done

      id="${config.cri.salt.id}-$(hostname -f)-''${ip}"
      sed -i '/^id:/d' /etc/salt/minion
      echo -e "\nid: $id" >> /etc/salt/minion
      echo "$id" > /etc/salt/minion_id
    '';
  };
}
